from time import sleep
import numpy as np
import pandas as pd
from tqdm import tqdm
import random
import argparse
from datetime import datetime
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException, WebDriverException


class Scraper:
    def __init__(self, creds, filename, chunk_size=10, start_chunk=0, chunk_wait=60):
        self.driver = webdriver.Firefox()
        self.creds = creds
        self.df = self.readData(filename)
        self.chunk_size = chunk_size
        self.start_chunk = start_chunk
        self.chunk_wait = chunk_wait
        self.login_url = 'https://www.airdna.co/vacation-rental-data/app/login'
        self.url = 'https://www.airdna.co/vacation-rental-data/app/it/'
        self.title = 'AirDNA MarketMinder'

    def run(self):
        self.login()
        self.scrape()
        self.quit()

    def readData(self, filename, verbose=False):
        df = pd.read_excel('data/'+filename, header=1, skiprows=1)
        df.drop(df.index[:2], inplace=True)
        df.reset_index(drop=True, inplace=True)
        df = df.dropna(subset=['Comune'])
        if verbose:
            print(df.info(), df.head())
        return df

    def login(self):
        print('Logging in...')
        self.driver.get(self.login_url)
        assert self.driver.title == self.title, f'page not available, got {driver.title}'
        self.driver.implicitly_wait(2.5)

        user_box = self.driver.find_element(by=By.ID, value="login-email")
        pwd_box = self.driver.find_element(by=By.ID, value="login-password")
        user_box.send_keys(self.creds['user'])
        pwd_box.send_keys(self.creds['pwd'])

        login_button = self.driver.find_element(by=By.CSS_SELECTOR, value="button[type='submit']")
        login_button.click()
        print('Logged in')

    def scrape(self):
        num_chunks = len(self.df) // self.chunk_size + (1 if len(self.df) % self.chunk_size != 0 else 0)
        for chunk_idx in tqdm(range(self.start_chunk, num_chunks), desc='Processing chunks...'):
            start_idx = chunk_idx * self.chunk_size
            end_idx = min((chunk_idx + 1) * self.chunk_size, len(self.df))
            chunk_df = self.df.iloc[start_idx:end_idx].copy()

            chunk_df = chunk_df.progress_apply(func=self.scrapeOne, axis=1, result_type='expand')
            self.df.loc[chunk_df.index, ['rental_demand', 'average_daily_rate', 'occupancy_rate', 'revenue', 'entire_home_rentals_percent', 'found']] = chunk_df[['rental_demand', 'average_daily_rate', 'occupancy_rate', 'revenue', 'entire_home_rentals_percent', 'found']]
            self.df[start_idx:end_idx].to_csv(f'data/chunks/chunk_{chunk_idx+1}.csv')
            sleep(self.chunk_wait)
            self.driver.get(self.driver.current_url) # reload

    def scrapeChunk(self, chunk):
        chunk[['rental_demand', 'average_daily_rate', 'occupancy_rate', 'revenue', 'entire_home_rentals_percent', 'found']] = chunk.progress_apply(func=self.scrapeOne, axis=1, result_type='expand')
        return self.df

    def scrapeOne(self, row):
        sleep(2)
        res = {'rental_demand': np.NaN, 'average_daily_rate': np.NaN, 'occupancy_rate': np.NaN, 'revenue': np.NaN, 'entire_home_rentals_percent': np.NaN, 'found': np.NaN}
        data = row.tolist()
        try:
            input_element = self.driver.find_element(by=By.ID, value='react-select-3-input')
            # Clear the existing text
            input_element.clear()
            # Enter the desired text
            search_str = data[2] + ', IT'
            self.typewrite(search_str, input_element)

            WebDriverWait(self.driver, 30).until_not(EC.text_to_be_present_in_element((By.CLASS_NAME, 'location-bar__city-select'), 'Loading...'))
            # Now, the text has changed or disappeared, indicating that the search results are ready.
            sleep(2) # give time for mid react frontend to update
            container_element = self.driver.find_element(By.CLASS_NAME, 'location-bar__city-select')
            search_results_text = container_element.text.split('\n')
            if search_results_text[0] == ' No results found':
                return res
            elif search_results_text[-1].lower() == search_str.lower():
                input_element.send_keys(Keys.ENTER) # go to found page
                WebDriverWait(self.driver, 30).until(lambda driver: driver.execute_script("return document.readyState") == "complete")
                sleep(0.3)
                return pd.Series(self.scrapeData(res))
            else:
                # weird cases / other not found
                return res

        except TimeoutException:
            print(f'Timeout on {data}')
            #raise TimeoutException

        except WebDriverException:
            print(f'WebDriver Exception on {data}')
            #raise WebDriverException

    def scrapeData(self, res, wait=20):
        try:
            rental_demand_xpath = "/html/body/div[2]/section/div/div/div/div/div[2]/div/div[1]/div/div/section[2]/div[3]/p"
            rental_demand_element = WebDriverWait(self.driver, wait).until(EC.visibility_of_element_located((By.XPATH, rental_demand_xpath)))
            res['rental_demand'] = rental_demand_element.text
            # average daily rate
            average_daily_rate_xpath = "/html/body/div[2]/section/div/div/div/div/div[2]/div/div[2]/div/div[1]/div[1]/div[1]"
            average_daily_rate_element =  WebDriverWait(self.driver, wait).until(EC.visibility_of_element_located((By.XPATH, average_daily_rate_xpath)))
            res['average_daily_rate'] = int(average_daily_rate_element.text[1:].replace(',', ''))
            # occupancy rate
            occupancy_rate_xpath = "/html/body/div[2]/section/div/div/div/div/div[2]/div/div[2]/div/div[2]/div[1]/div[1]"
            occupancy_rate_element = WebDriverWait(self.driver, wait).until(EC.visibility_of_element_located((By.XPATH, occupancy_rate_xpath)))
            res['occupancy_rate'] = int(occupancy_rate_element.text[:1])
            # revenue
            revenue_xpath = "/html/body/div[2]/section/div/div/div/div/div[2]/div/div[2]/div/div[3]/div[1]/div[1]"
            revenue_element = WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located((By.XPATH, revenue_xpath)))
            res['revenue'] = int(revenue_element.text[1:].replace(',', ''))
            # active rentals
            active_rentals_xpath = "/html/body/div[2]/section/div/div/div/div/div[2]/div/div[3]/div[1]/div[1]/div[1]"
            active_rentals_element = WebDriverWait(self.driver, wait).until(EC.visibility_of_element_located((By.XPATH, active_rentals_xpath)))
            res['revenue'] = int(active_rentals_element.text.split(' ')[0].replace(',', ''))
            # entire home rentals percentage
            entire_home_rentals_xpath = "/html/body/div[2]/section/div/div/div/div/div[2]/div/div[3]/div[1]/h3/span[2]"
            entire_home_rentals_element = WebDriverWait(self.driver, wait).until(EC.visibility_of_element_located((By.XPATH, entire_home_rentals_xpath)))
            res['entire_home_rentals_percent'] = int(entire_home_rentals_element.text.split('%')[0])
            res['found'] = 1
            return pd.Series(res)

        except Exception as e:
            print(f'exception: {e} happened while pulling data')
            res['found'] = 2
            return pd.Series(res)

    def typewrite(self, s, element, delay=0.02):
        for char in s:
            random_var = random.uniform(-0.01, 0.03 )
            element.send_keys(char)
            sleep(delay + random_var)

    def quit(self):
        self.df.to_csv(f'data/results_{datetime.now().strftime("%Y-%m-%d_%H-%M-%S")}.csv')
        print(self.df.tail(6))
        print(self.df.info())
        found = self.df['found'].eq(1).sum()
        error = self.df['found'].eq(2).sum()
        not_found= self.df['found'].isnull().sum()
        print(f'Scraping completed\nfound: {found}\nnot found: {not_found}\nerror while scraping: {error}\n')
        print('Quitting...')
        self.driver.quit()


def parser():
    parser = argparse.ArgumentParser(description='AirDNA Scraper')
    parser.add_argument('--filename', type=str, default='lista comuni.xlsx', help='Input Excel filename')
    parser.add_argument('--chunk-size', type=int, default=100, help='Chunk size')
    parser.add_argument('--start-chunk', type=int, default=0, help='Start chunk index to resume scraping')
    parser.add_argument('--chunk-wait', type=int, default=60, help='Seconds to idle inbetween batches')
    parser.add_argument('--user', type=str, required=True, help='AirDNA username')
    parser.add_argument('--pwd', type=str, required=True, help='AirDNA password')
    args = parser.parse_args()
    creds = {'user': args.user, 'pwd': args.pwd}
    return args, creds


if __name__ == '__main__':
    args, creds = parser()
    tqdm.pandas(desc='Scraping...')
    s = Scraper(creds, filename=args.filename, chunk_size=args.chunk_size, start_chunk=args.start_chunk)
    s.run()
